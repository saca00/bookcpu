extern crate ncurses;
use ncurses::*;
use std::env;
use std::io;
use std::io::prelude::*;
use std::fs::File;
fn main() -> io::Result<()> {
  println!("BookCPU Emulator\n");
  let args: Vec<String> = env::args().collect();
  let mut memory: [u16; 4096] = [0; 4096];
  let mut counter: usize = 0;
  let mut register: u16 = 0;
  let mut gt: bool = false;
  let mut eq: bool = false;
  let mut lt: bool = false;
  let mut opcode: u8;
  let mut address: usize;
  let mut contents: u16;
  let mut filename: String = "program".to_string();
  if args.len() > 1 {filename = args[1].clone();}
  let mut program = File::open(filename)?;
  let mut buffer: [u8; 8192] = [0; 8192];
  program.read(&mut buffer)?;
  let mut j = 0;
  for i in 0..4096 {
    memory[i] = ((buffer[j] as u16) << 8) | buffer[j + 1] as u16;
    j += 2;
  }
  initscr();
  loop {
    opcode = (memory[counter] >> 12) as u8;
    address = (memory[counter] & 0x0FFF) as usize;
    match opcode {
      0 => register = memory[address],
      1 => memory[address] = register,
      2 => memory[address] = 0,
      3 => register += memory[address],
      4 => memory[address] += 1,
      5 => register -= memory[address],
      6 => memory[address] -= 1,
      7 => {
        contents = memory[address];
        gt = contents > register;
        eq = contents == register;
        lt = contents < register;
      },
      8 => {counter = address},
      9 => if gt {counter = address},
      10 => if eq {counter = address},
      11 => if lt {counter = address},
      12 => if !eq {counter = address},
      13 => memory[address] = getch() as u16,
      14 => print!("{}", memory[address]as u8 as char),
      _ => break,
    }
    if opcode < 8 || opcode > 12 {counter += 1;}
    if counter > 4095 {break};
  }
  endwin();
  println!("\n\nProgram complete, system halted at adress {}", counter);
  Ok(())
}
